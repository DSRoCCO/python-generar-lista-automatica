import unittest
import random
from unittest.mock import patch
from random_list import *


@patch('random.randint')
class RandomListTests(unittest.TestCase):

	def test_random_list_example1(self, mocked_randint):
		mocked_randint.return_value = range(5, 11)
		expected = [['Robert', [range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11)]], ['Charlis', [range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11)]], ['Marco', [range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11)]], ['Pepe', [range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11)]], ['Angi', [range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11), range(5, 11)]]]
		self.assertEqual(random_list(["Robert", "Charlis", "Marco", "Pepe", "Angi"], 7, 5, 10), expected)
		self.assertTrue(mocked_randint.called)
		mocked_randint.assert_called_with(5, 10)
	
	def test_random_list_example2(self, mocked_randint):
		mocked_randint.return_value = range(1, 11)
		expected = [['Faby', [range(1, 11), range(1, 11), range(1, 11), range(1, 11), range(1, 11)]], ['Moni', [range(1, 11), range(1, 11), range(1, 11), range(1, 11), range(1, 11)]], ['Stefani', [range(1, 11), range(1, 11), range(1, 11), range(1, 11), range(1, 11)]], ['Herni', [range(1, 11), range(1, 11), range(1, 11), range(1, 11), range(1, 11)]]]
		self.assertEqual(random_list(["Faby", "Moni", "Stefani", "Herni"], 5, 1, 10), expected)
		self.assertTrue(mocked_randint.called)
		mocked_randint.assert_called_with(1, 10)

if __name__=="__main__":
    unittest.main()